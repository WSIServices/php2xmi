<?php

namespace PHP2XMI;

class XmiClassExtends 
{ 
    public function __construct($className, $otherClassName)
    {
        $this->_childName = $className;
        $this->_parentName = $otherClassName;
    }

    public function getChild(){ return $this->_childName; }
    public function getParent(){ return $this->_parentName; }

    private $_childName;
    private $_parentName;
}
