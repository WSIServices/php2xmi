<?php

namespace PHP2XMI;

class XmiNameSpaceResolver
{
    private $classes = array();

    public function addFile($file)
    {
        $tokens = token_get_all(file_get_contents($file));
        $count = count($tokens);

        $classes = array();
        $namespace = null;
        $aliases = array();
        for ($i = 0 ; $i < $count ; $i++)
        {
            $token = $tokens[$i];
            if ($token[0] === T_USE)
            {
                $status = 0;
                $name = '';
                $alias = '';
                do {
                    $token = $tokens[++$i];
                    switch($token[0])
                    {
                        case T_WHITESPACE:
                            break;
                        case T_AS:
                            $status = 1;
                            break;
                        case T_NS_SEPARATOR:
                        case T_STRING:
                            if (0 === $status) {
                                $name .= $token[1];
                            } else {
                                $alias .= $token[1];
                            }
                            break;
                        case ',':
                        case ';':
                            if (!$alias) {
                                $arr = explode('\\', $name);
                                $alias = array_pop($arr);
                            }
                            $aliases[$alias] = $name;
                            $status = 0;
                            $alias = '';
                            $name = '';
                            break;
                    }

                } while($token[0] != ';');
            } elseif($token[0] == T_NAMESPACE) {
                $namespace = '';
                do {
                    $token = $tokens[++$i];
                    switch($token[0])
                    {
                        case T_NS_SEPARATOR:
                        case T_STRING:
                            $namespace .= $token[1];
                    }
                } while($token[0] != ';');
            } elseif($token[0] == T_INTERFACE || $token[0] == T_CLASS) {
                $class = '';
                do {
                    $token = $tokens[++$i];
                    switch($token[0])
                    {
                        case T_WHITESPACE:
                            break;
                        case T_NS_SEPARATOR:
                        case T_STRING:
                            $classes[] = $token[1];
                            break 2;
                    }
                } while($token[0] != ';');
            }
        }
        foreach ($classes as $class)
        {
            if ($namespace)
            {
                $class = $namespace . '\\' . $class;
            }
            $this->classes[$class] = array('namespace' => $namespace, 'use' => $aliases);
        }
    }

    public function getClassInfo($class)
    {
        return $this->classes[$class];
    }

    public function getFullyQualifiedClassName($context, $classname)
    {
        if (!preg_match('/^(?:boolean|bool|string|str|integer|int|float|array|mixed|callback)$/i', $classname))
        {
            if ('\\' !== $classname[0]) {
                $explode = explode('\\', $classname, 2);
                if (isset($this->classes[$context]['use'][$explode[0]])) {
                    $classname = $this->classes[$context]['use'][$explode[0]];
                    if (isset($explode[1])) $classname .= '\\' . $explode[1];
                } elseif (isset($this->classes[$context]['namespace'])) {
                    $classname = $this->classes[$context]['namespace'] . '\\' . $classname;
                }
            }
            if ('\\' === $classname[0]) $classname = substr($classname, 1);
        }
        return $classname;
    }
}
