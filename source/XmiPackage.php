<?php

namespace PHP2XMI;

class XmiPackage
{
    public function __construct(XmiWriter $writer, $name)
    {
        $this->_writer = $writer;
        $this->_name = $name;
        $this->_classes = array();
        $this->_packages = array();
        $this->_parent = null;
    }

    public function getName()
    {
        if ($this->_parent != null){
            return $this->_parent->getName().'.'.$this->_name;
        }
        return $this->_name;
    }

    public function addPackage(XmiPackage $package)
    {
        $this->_packages[$package->getName()] = $package;
        $package->_parent = $this;
    }

    public function hasPackage($name)
    {
        return array_key_exists($name, $this->_packages);
    }

    public function getPackage($name)
    {
        return $this->_packages[$name];
    }

    public function addClass(XmiClassWriter $class)
    {
        array_push($this->_classes, $class);
    }

    public function write()
    {
        $this->writeHead();
        foreach ($this->_packages as $name => $package){
            $package->write();
        }
        foreach ($this->_classes as $class){
            $this->_writer->writeData($class->getXmi());
        }
        $this->writeFoot();
    }

    private function writeHead()
    {
        $this->_writer->writeData('<UML:Package visibility="public" xmi.id="',$this->_writer->nextXmiId(),'" name="package.',htmlspecialchars($this->_name),'">',"\n");
        $this->_writer->writeData('<UML:Namespace.ownedElement>',"\n");
    }

    private function writeFoot()
    {
        $this->_writer->writeData('</UML:Namespace.ownedElement>',"\n");
        $this->_writer->writeData('</UML:Package>',"\n");
    }

    private $_packages;
    private $_classes;
    private $_writer;
    private $_name;
}
