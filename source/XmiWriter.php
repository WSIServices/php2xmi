<?php

namespace PHP2XMI;

class XmiWriter
{
    const ID_MYSTEREO  = 4;
    const ID_DATATYPE  = 5;
    const ID_INTERFACE = 6;
    const ID_START     = 7;

    public function __construct()
    {
        $this->_xmiId = self::ID_START;
        $this->_packages = array();
        $this->_classes = array();
        $this->_classIds = array();
        $this->_types = array();
        $this->_extends = array();
        $this->_implements = array();
        $this->_showPrivate = true;
        $this->_showProtected = true;
        $this->_showPublic = true;
        $this->_buffer = '';
        $this->_nsResolver = null;
    }

    public function writeData()
    {
        $args = func_get_args();
        $this->_buffer .= implode('', $args);
    }

    public function enablePrivate($bool)
    {
        $this->_showPrivate = $bool;
    }

    public function enableProtected($bool)
    {
        $this->_showProtected = $bool;
    }


    public function enablePublic($bool)
    {
        $this->_showPublic = $bool;
    }

    public function setNSResolver($nsr)
    {
        $this->_nsResolver = $nsr;
    }

    public function getNSResolver()
    {
        return $this->_nsResolver;
    }

    public function acceptPrivate()
    {
        return $this->_showPrivate;
    }

    public function acceptPublic()
    {
        return $this->_showPublic;
    }


    public function acceptProtected()
    {
        return $this->_showProtected;
    }

    public function addClass($className)
    {
        $this->_classes[$className] = null;
    }

    public function write()
    {
        foreach ($this->_classes as $name => $false){
            $this->prepareClass(new \ReflectionClass($name));
        }
        $this->writeHead();
        $this->writeDataTypes();
        $this->writePackages();
        $this->writeClasses();
        foreach ($this->_extends as $ext){
            $this->writeClassExtends($ext);
        }
        foreach ($this->_implements as $ext){
            $this->writeClassImplements($ext);
        }
        $this->writeFoot();
        return $this->_buffer;
    }

    private function writeClassExtends(XmiClassExtends $ext)
    {
        $this->writeData('<UML:Generalization child="', $this->getTypeId($ext->getChild()),'" visibility="public" xmi.id="',$this->nextXmiId(),'" parent="',$this->getTypeId($ext->getParent()),'" />',"\n");
    }

    private function writeClassImplements(XmiClassImplements $ext)
    {
        $this->writeData('<UML:Generalization child="', $this->getTypeId($ext->getClassName()),'" visibility="public" xmi.id="',$this->nextXmiId(),'" parent="',$this->getTypeId($ext->getInterfaceName()),'" />',"\n");
    }

    public function nextXmiId()
    {
        return ++$this->_xmiId;
    }

    public function addClassExtends(XmiClassExtends $ext)
    {
        $this->_extends[] = $ext;
    }

    public function addClassImplements(XmiClassImplements $ext)
    {
        $this->_implements[] = $ext;
    }

    private function prepareClass(\ReflectionClass $class)
    {
        $this->registerClass($class->getName());
        if ($class->isInterface()){
            $writer = new XmiInterfaceWriter($this, $class);
        }
        else {
            $writer = new XmiClassWriter($this, $class);
        }
        $writer->write();
        $this->_classes[$class->getName()] = $writer;
    }

    private function writeClasses()
    {
        foreach ($this->_classes as $name => $classWriter){
            if ($classWriter != null && !$classWriter->isPackaged())
                $this->writeData($classWriter->getXmi());
        }
    }

    private function writeHead()
    {
        $this->writeData('<?xml version="1.0" encoding="UTF-8" ?>',"\n");
        $this->writeData('<XMI xmlns:UML="org.omg/standards/UML" verified="false" timestamp="" xmi.version="1.2">', "\n");
        $this->writeData('<XMI.header>',"\n");
        $this->writeData('<XMI.documentation>', "\n");
        $this->writeData('<XMI.exporter>PHP2XMI</XMI.exporter>', "\n");
        $this->writeData('<XMI.exporterVersion>1.0</XMI.exporterVersion>',"\n");
        $this->writeData('<XMI.exporterEncoding>UnicodeUTF8</XMI.exporterEncoding>',"\n");
        $this->writeData('</XMI.documentation>',"\n");
        $this->writeData('<XMI.model xmi.name="php2xmi" />',"\n");
        $this->writeData('<XMI.metamodel xmi.name="UML" href="UML.xml" xmi.version="1.3" />',"\n");
        $this->writeData('</XMI.header>',"\n");
        $this->writeData('<XMI.content>',"\n");
        $this->writeData('<UML:Model>', "\n");
        $this->writeData('<UML:Namespace.ownedElement>',"\n");
        $this->writeData('<UML:Stereotype visibility="public" xmi.id="',self::ID_MYSTEREO,'" name="my-stereotype" />',"\n");
        $this->writeData('<UML:Stereotype visibility="public" xmi.id="',self::ID_DATATYPE,'" name="datatype" />', "\n");
        $this->writeData('<UML:Stereotype visibility="public" xmi.id="',self::ID_INTERFACE,'" name="interface" />', "\n");
    }

    private function writePackages()
    {
        foreach ($this->_packages as $name => $package){
            $package->write();
        }
    }

    private function writeDataTypes()
    {
        foreach ($this->_types as $type => $id){
            if (!array_key_exists($type, $this->_classIds)){
                $this->writeDataType($id, $type);
            }
        }
    }

    public function getTypeId($type)
    {
        if (!array_key_exists($type, $this->_types))
            $this->_types[$type] = $this->nextXmiId();
        return $this->_types[$type];
    }

    public function getPackage($name)
    {
        $parts = explode('.', $name);
        $rootName = array_shift($parts);
        if (!array_key_exists($rootName, $this->_packages)){
            $this->_packages[$rootName] = new XmiPackage($this,$rootName);
        }
        $package = $this->_packages[$rootName];

        foreach ($parts as $part){
            if (!$package->hasPackage($part)){
                $child = new XmiPackage($this, $part);
                $package->addPackage($child);
            }
            $package = $package->getPackage($part);
        }

        return $package;
    }

    private function registerClass($className)
    {
        $id = $this->getTypeId($className);
        $this->_classIds[$className] = $id;
    }

    private function writeDataType($id, $name)
    {
        $this->writeData('<UML:DataType stereotype="',self::ID_DATATYPE,'" visibility="public" xmi.id="',$id,'" name="',htmlspecialchars($name),'"/>',"\n");
    }

    private function writeFoot()
    {         
        $this->writeData('</UML:Namespace.ownedElement>',"\n");
        $this->writeData('</UML:Model>',"\n");
        $this->writeData('</XMI.content>',"\n");
        $this->writeData('</XMI>');
    }

    public static function extractReturnTypeFromComment($comment)
    {
        if (preg_match('/@return\s+(\S+)/', $comment, $m)){
            return $m[1];
        }
        return 'void';
    }

    public static function extractParamTypeFromComment($comment, $name)
    {
        if (preg_match('/@param\s+\$?'.$name.'\s+(\S+)/', $comment, $m))
            return $m[1];
        if (preg_match('/@param\s+(\S+)\s+\$?'.$name.'/', $comment, $m))
            return $m[1];
        return 'mixed';
    }

    public static function extractMemberTypeFromComment($comment)
    {
        if (preg_match('/@(?:type|var)\s+(\S+)/', $comment, $m)){
            return $m[1];
        }
        return 'mixed';
    }

    public static function extractPackageNameFromComment($comment)
    {
        if (preg_match('/@package\s+(\S+)/', $comment, $m)){
            return $m[1];
        }
        return '';
    }

    private $_packages;
    private $_extends;
    private $_implements;
    private $_showPrivate;
    private $_showProtected;
    private $_showPublic;
    private $_buffer;
    private $_classes;
    private $_xmiId;
    private $_classIds;
    private $_types;
    private $_nsResolver;
}
