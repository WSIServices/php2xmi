<?php

namespace PHP2XMI;

class XmiInterfaceWriter extends XmiClassWriter
{
    protected function writeHead()
    {
        $this->writeData('<UML:Interface visibility="public" xmi.id="',$this->getId(),'" isAbstract="true" name="',htmlspecialchars($this->_class->getName()),'">', "\n");
        $this->writeData('<UML:Classifier.feature>',"\n");
    }

    protected function writeFoot()
    {
        $this->writeData('</UML:Classifier.feature>',"\n");
        $this->writeData('</UML:Interface>',"\n");
    }
}
