<?php

namespace PHP2XMI;

class XmiClassWriter 
{
    public function __construct(XmiWriter $writer, \ReflectionClass $class)
    {
        $this->_writer = $writer;
        $this->_class = $class;
        $this->_xmi = '';
        $this->_packaged = false;
    }

    public function isPackaged(){
        return $this->_packaged;
    }

    protected function getId()
    {
        return $this->_writer->getTypeId($this->_class->getName());
    }

    protected function writeData()
    {
        $args = func_get_args();
        $this->_xmi .= implode('', $args);
    }

    public function write()
    {
        $parentClass = $this->_class->getParentClass();
        if ($parentClass != null){
            $this->_writer->addClassExtends(new XmiClassExtends($this->_class->getName(), $parentClass->getName()));
        }
        foreach ($this->_class->getInterfaces() as $interface){
            if ($parentClass == null || !$parentClass->implementsInterface($interface->getName())){
                $this->_writer->addClassImplements(new XmiClassImplements($this->_class->getName(), $interface->getName()));
            }
        }

        $packageName = XmiWriter::extractPackageNameFromComment($this->_class->getDocComment());
        if ($packageName){
            $package = $this->_writer->getPackage($packageName);
            $package->addClass($this);
            $this->_packaged = true;
        }

        $this->writeHead();
        $this->writeMembers();
        $this->writeMethods();
        $this->writeFoot();
    }

    public function getXmi()
    {
        return $this->_xmi;
    }

    public function getTypeId($name)
    {
        return $this->_writer->getTypeId($name);
    }

    protected function writeHead()
    {
        $this->writeData('<UML:Class visibility="public" xmi.id="',$this->getId(),'" name="',$this->_class->getName(),'"');
        if ($this->_class->isAbstract()) $this->writeData(' isAbstract="true"');
        $this->writeData('>', "\n");
        $this->writeData('<UML:Classifier.feature>',"\n");
    }

    protected function writeFoot()
    {
        $this->writeData('</UML:Classifier.feature>',"\n");
        $this->writeData('</UML:Class>',"\n");
    }

    protected function getVisibility(\Reflector $o)
    {
        if ($o->isPublic()) return 'public';
        if ($o->isPrivate()) return 'private';
        if ($o->isProtected()) return 'protected';
    }

    protected function nextXmiId()
    {
        return $this->_writer->nextXmiId();
    }

    protected function writeMembers()
    {
        foreach ($this->_class->getProperties() as $prop){
            $this->writeProperty($prop);
        }
    }

    protected function writeProperty(\ReflectionProperty $property)
    {
        if (!$this->_writer->acceptPrivate() && $property->isPrivate()) return;
        if (!$this->_writer->acceptProtected() && $property->isProtected()) return;
        if (!$this->_writer->acceptPublic() && $property->isPublic()) return;

        // ignore parent properties
        if ($property->getDeclaringClass() != $this->_class){
            return;
        }

        if (version_compare(phpversion(), '5.1.0', '>=')){
            // only for PHP 5.1.0 implements ReflectionProperty::getDocComment()
            $type = XmiWriter::extractMemberTypeFromComment($property->getDocComment());
        }
        else  {           
            $type = 'mixed'; // damn it
        }

        $type = $this->_writer->getNSResolver()->getFullyQualifiedClassName($this->_class->name, $type);
        $id = $this->getTypeId($type);

        $this->writeData('<UML:Attribute visibility="',$this->getVisibility($property),'" xmi.id="',$this->nextXmiId(),'" value="" type="',$id,'" name="',htmlspecialchars($property->getName()),'"');
        if ($property->isStatic()) $this->writeData(' ownerScope="classifier"');
        $this->writeData('/>', "\n");
    }

    protected function writeMethods()
    {
        foreach ($this->_class->getMethods() as $method){
            $this->writeMethod($method);
        }
    }

    protected function writeMethod(\ReflectionMethod $method)
    {
        if (!$this->_writer->acceptPrivate() && $method->isPrivate()) return;
        if (!$this->_writer->acceptProtected() && $method->isProtected()) return;
        if (!$this->_writer->acceptPublic() && $method->isPublic()) return;

        // ignore parent methods
        if ($method->getDeclaringClass() != $this->_class){
            return;
        }

        $type = XmiWriter::extractReturnTypeFromComment($method->getDocComment());
        $type = $this->_writer->getNSResolver()->getFullyQualifiedClassName($this->_class->name, $type);

        $id = $this->getTypeId($type);
        $this->writeData('<UML:Operation visibility="',$this->getVisibility($method),'" xmi.id="',$this->nextXmiId(),'" type="',$id,'" name="',$method->getName(),'"');
        if ($method->isAbstract()) $this->writeData(' isAbstract="true"');
        if ($method->isStatic()) $this->writeData(' ownerScope="classifier"');
        $this->writeData('>', "\n");

        $this->writeData('<UML:BehavioralFeature.parameter>',"\n");
        foreach ($method->getParameters() as $param){
            $this->writeMethodParam($method, $param);
        }
        $this->writeData('</UML:BehavioralFeature.parameter>',"\n");

        $this->writeData('</UML:Operation>', "\n");
    }

    protected function writeMethodParam(\ReflectionMethod $method, \ReflectionParameter $param)
    {
        // $param->getDefaultValue() sometimes makes php crash this it is
        // deactivated until PHP reflection is fixed
        // function foo($var=self::CONST_VALUE);
        $default = $param->isOptional() ? $param->getDefaultValue() : '';

        try {
            $paramClass = $param->getClass();
        }
        catch (\ReflectionException $e){
            // warning ? param class not included
            $paramClass = null;
        }
        if ($paramClass != null){
            $type = $paramClass->getName();
        }
        else {
            $type = XmiWriter::extractParamTypeFromComment($method->getDocComment(), $param->getName());
            $type = $this->_writer->getNSResolver()->getFullyQualifiedClassName($this->_class->name, $type);
        }
        $id = $this->getTypeId($type);
        if (is_array($default)) $default = 'array()';
        $this->writeData('<UML:Parameter visibility="public" xmi.id="',$this->nextXmiId(),'" value="',htmlspecialchars($default),'" type="',$id,'" name="',htmlspecialchars($param->getName()),'"/>', "\n");
    }

    protected $_writer;
    protected $_class;
    protected $_xmi;
    protected $_packaged;
}
