<?php

namespace PHP2XMI;

class XmiClassImplements
{ 
    public function __construct($className, $interfaceName)
    {
        $this->_className = $className;
        $this->_interfaceName = $interfaceName;
    }

    public function getClassName() { return $this->_className; }
    public function getInterfaceName(){ return $this->_interfaceName; }

    private $_className;
    private $_interfaceName;
}
