#PHP2XMI

Fork of php2xmi with Namespace support.

The original authored by: Laurent Bedubourg <lbedubourg@motion-twin.com>
http://tech.motion-twin.com/php_php2xmi.html

Namespace adaption authored by: Charron Pierrick <pierrick@webstart.fr>
https://github.com/adoy/php2xmi-ns

>This PHP console script generates an XMI scheme (1.3) representing your classes and interfaces.
>
>The XMI scheme can be imported into UML modelers like umbrello to browse, print, think about your library/application design.
>
>The PHP2XMI script is licensed under the terms of the LGPL (GNU LESSER GENERAL PUBLIC LICENSE) contained in the COPYING file of this package.


You can read the [original README][original-readme] for more details. Changes are mentioned below.


###DOWNLOAD

Use git to install it.

    git clone https://bitbucket.org/WSIServices/php2xmi.git


###USAGE

The default behaviour is to output the XMI scheme to stdout, you may choose to redirect stdout to some result file (won't warn you about php errors) or better use the `--output=<filename>` parameter.

    To get the list of available command line parameters:

    php2xmi --help

    Example with only some specified files:

    php2xmi \
    --ouput=myresult.xmi \
    Foo.php Bar.php

    Example using a library repository and the --recursive argument:

    php2xmi \
    --no-public \
    --ouput=myresult.xmi \
    --recursive \
    /home/user/website/lib

	Example using a library, the --recursive argument, and an autoload directory

	php2xmi \
	--autoload=/home/user/website/vendors \
	--no-private \
	--no-protected \
	--recursive \
	/home/user/website/lib

    Example using linux find tool:

    php2xmi \
    --path=/home/user/website/lib:/home/user/website/misclib \
    --no-private \
    --no-protected \
    --ouput=myresult.xmi \
    `find /home/user/website/lib -name "*.php"`

