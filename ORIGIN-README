#PHP2XMI

_This files content was copied directly from [Motion-Twin Techs][motion-twin-tech] page on [PHP2XMI][motion-twin-php2xmi] on Feburary 20th 2013._

>This PHP console script generates an XMI scheme (1.3) representing your classes and interfaces.
>
>The XMI scheme can be imported into UML modelers like umbrello to browse, print, think about your library/application design.
>
>The PHP2XMI script is licensed under the terms of the LGPL (GNU LESSER GENERAL PUBLIC LICENSE) contained in the COPYING file of this package.


###FEATURES

* public, protected, private, abstract, static, interface, extends, implements
* method argument type discovery through php5 type hinting
* @package support (package and subpackages separated by dots)
* @param type support
* @return type support
* @type attribute support (PHP 5.1.X only)
* Uses PHP5 builtin reflection which gives good performances


###KNOWN LIMITATIONS

Because php2xmi uses PHP5 reflection facilities, it must include your php files to do its work. If your files are missing some require_onces or are doing something in their main body, php2xmi may crash or you may experience strange behaviour (database connection, filewriting, etc...). For this reason, it is not recommended to run php2xmi on your PEAR directory which contains scripts and tests.

Because PHP5.0.X does not implements the ReflectionAttribute::getDocComment() method, the type of class attributes cannot be determined using the PHP5 reflection system. PHP2XMI already contains the php version check which will automatically toggle this feature on when run using PHP >= 5.1.X.

Because of the last limitation, associations, compositions, aggregations, ... won't be supported nor implemented until PHP 5.1 is publicly released.

Because there are many XMI versions, the produced scheme may not be importable with every UML modelers on the market. However, it works just fine with __[umbrello]__ which is free. PHP2XMI was developped using this modeler as a reference (even if I run a gnome desktop and would have prefered a gtk modeler:).


###DOWNLOAD

Use pear to install it.

    pear install http://tech.motion-twin.com/zip/PHP2XMI-0.1.2.tar.gz


###USAGE

The default behaviour is to output the XMI scheme to stdout, you may choose to redirect stdout to some result file (won't warn you about php errors) or better use the `--output=<filename>` parameter.

    To get the list of available command line parameters :

    php2xmi --help

    Example with only some specified files :

    php2xmi \
    --path=/home/user/website/lib:/home/user/website/misclib \
    --ouput=myresult.xmi \
    Foo.php Bar.php

    Example using a library repository and the --recursive argument :

    php2xmi \
    --path=/home/user/website/lib:/home/user/website/misclib \
    --no-private \
    --ouput=myresult.xmi \
    --recursive \
    /home/user/website/lib

    Example using linux find tool : 

    php2xmi \
    --path=/home/user/website/lib:/home/user/website/misclib \
    --no-private \
    --no-protected \
    --ouput=myresult.xmi \
    `find /home/user/website/lib -name "*.php"`


###PHP DOCUMENTATION KEYWORDS

Here is a commented example of how you may document your code to generate nearly fully typed XMI schemes :

    /**
     * @package PackName.SubPack
     */
    class Foo {
        /**
    	 * This requires PHP 5.1.X to to work (not tested yet).
    	 * @type int
    	 */
        public $myInt;

    	/**
    	 * $aFoo type may or may not be commented with a @param, its 
    	 * type is already found in the method declaration.
    	 *
    	 * Following syntax are recognized :
    	 *
    	 * @param $someInt int
    	 * @param string $someString That is a string
    	 * @param someInt int Still an int
    	 * @param string someString Well ...
    	 *
    	 * And the return type may be something like :
    	 * @return array<Foo> An array containing the $aFoo parameter
    	 *
    	 * Unless you prefers :
    	 * @return array
    	 */
    	public function doFoo(Foo $aFoo, $someInt, $someString)
    	{
    		return array($aFoo);
    	}
    }


###EXAMPLE

Here is a screenshot of umbrello with a generated phptal.xmi file opened.

[![phptal.xmi on umbrello][umbrello-phptal.xmi-thumb]][umbrello-phptal.xmi]

The phptal.xmi file was generated using the following command :

    php2xmi --no-private --no-protected --recursive PHPTAL.php /usr/lib/php/PHPTAL

__Note:__ PHPTAL.php does not include all its package files but only the strict necessary, that why I had to add PHPTAL package directory to the list of files and toggle the `--recursive` parameter.

On the left, umbrello presents the tree of all classes, interfaces and packages found in the XMI file.

The diagram on the right is obtained simply by draging some selected classes from the tree to the diagram.

People with good eyes will see many 'void' and 'mixed' because i did not put _@return_ and _@param_ in my method comments (shame on me:)


###NOTES

This script is young and was quickly written. Please do not expect too much miracles, it just try to do its job and most of the time succeed unless an included file trigger some buggy code.

The php2xmi contains some Xmi* classes which can be cleaned up and turned into a pear library if necessary (just ask me).

I've 'found' two other tools also named "php2xmi" which supposedly do the same thing as the Motion-Twin php2xmi script :

* [php2xmi][php2xmi-java] : written in java, support only php4 at this time and would require more effort to use in a PHP environment.
* [php2xmi][php2xmi-sitepoint] : i was not able to download this tool and i assumed it is not free nor maintained since the last post activity is dated from the Apr 27, 2004, 06:18

 [motion-twin-tech]: http://tech.motion-twin.com/
 [motion-twin-php2xmi]: http://tech.motion-twin.com/php_php2xmi.html
 [umbrello]: http://uml.sourceforge.net/index.php
 [umbrello-phptal.xmi-thumb]: ./documentation/images/umbrello-phptal.xmi-thumb.png
 [umbrello-phptal.xmi]: ./documentation/images/umbrello-phptal.xmi.png "phptal.xmi generated model on umbrello"
 [php2xmi-java]: https://php2xmi.dev.java.net/
 [php2xmi-sitepoint]: http://www.sitepoint.com/forums/showthread.php?t=166054